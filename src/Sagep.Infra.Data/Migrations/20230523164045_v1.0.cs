﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;
using Npgsql.EntityFrameworkCore.PostgreSQL.Metadata;

#nullable disable

#pragma warning disable CA1814 // Prefer jagged arrays over multidimensional

namespace Sagep.Infra.Data.Migrations
{
    /// <inheritdoc />
    public partial class v10 : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "AspNetRoles",
                columns: table => new
                {
                    Id = table.Column<string>(type: "text", nullable: false),
                    Description = table.Column<string>(type: "text", nullable: true),
                    Subject = table.Column<string>(type: "text", nullable: true),
                    Actions = table.Column<int[]>(type: "integer[]", nullable: true),
                    Name = table.Column<string>(type: "character varying(256)", maxLength: 256, nullable: true),
                    NormalizedName = table.Column<string>(type: "character varying(256)", maxLength: 256, nullable: true),
                    ConcurrencyStamp = table.Column<string>(type: "text", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetRoles", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Tenants",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uuid", nullable: false),
                    ApiKey = table.Column<Guid>(type: "uuid", nullable: false),
                    Nome = table.Column<string>(type: "character varying(255)", maxLength: 255, nullable: false),
                    NomeExibicao = table.Column<string>(type: "text", nullable: true),
                    OficioLeituraAssinaturaNome = table.Column<string>(type: "character varying(500)", maxLength: 500, nullable: true),
                    OficioLeituraAssinaturaCargo = table.Column<string>(type: "character varying(500)", maxLength: 500, nullable: true),
                    OficioLeituraAssinaturaMatricula = table.Column<string>(type: "character varying(500)", maxLength: 500, nullable: true),
                    OficioLeituraVocativo1 = table.Column<string>(type: "character varying(50)", maxLength: 50, nullable: true),
                    OficioLeituraVocativo2 = table.Column<string>(type: "character varying(500)", maxLength: 500, nullable: true),
                    OficioLeituraVocativo3 = table.Column<string>(type: "character varying(500)", maxLength: 500, nullable: true),
                    EnderecoLogradouro = table.Column<string>(type: "character varying(500)", maxLength: 500, nullable: true),
                    EnderecoLogradouroNumero = table.Column<string>(type: "character varying(10)", maxLength: 10, nullable: true),
                    EnderecoBairro = table.Column<string>(type: "text", nullable: true),
                    EnderecoCEP = table.Column<string>(type: "character varying(10)", maxLength: 10, nullable: true),
                    EnderecoCidade = table.Column<string>(type: "character varying(50)", maxLength: 50, nullable: true),
                    EnderecoEstado = table.Column<string>(type: "character varying(25)", maxLength: 25, nullable: true),
                    TelefoneDDD = table.Column<string>(type: "character varying(2)", maxLength: 2, nullable: true),
                    TelefoneNumero = table.Column<string>(type: "character varying(12)", maxLength: 12, nullable: true),
                    EmailPrincipal = table.Column<string>(type: "text", nullable: true),
                    IsDeleted = table.Column<bool>(type: "boolean", nullable: false),
                    CreatedAt = table.Column<DateTimeOffset>(type: "timestamp with time zone", nullable: false),
                    CreatedBy = table.Column<Guid>(type: "uuid", nullable: false),
                    UpdatedAt = table.Column<DateTimeOffset>(type: "timestamp with time zone", nullable: false),
                    UpdatedBy = table.Column<Guid>(type: "uuid", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Tenants", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "VerticalNavItems",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uuid", nullable: false),
                    Icon = table.Column<string>(type: "text", nullable: true),
                    Path = table.Column<string>(type: "text", nullable: true),
                    Title = table.Column<string>(type: "text", nullable: true),
                    Action = table.Column<string>(type: "text", nullable: true),
                    Subject = table.Column<string>(type: "text", nullable: true),
                    Disabled = table.Column<bool>(type: "boolean", nullable: false),
                    BadgeContent = table.Column<string>(type: "text", nullable: true),
                    ExternalLink = table.Column<bool>(type: "boolean", nullable: false),
                    OpenInNewTab = table.Column<bool>(type: "boolean", nullable: false),
                    BadgeColor = table.Column<string>(type: "text", nullable: true),
                    SectionTitle = table.Column<string>(type: "text", nullable: true),
                    Position = table.Column<int>(type: "integer", nullable: false),
                    LevelMeKey = table.Column<Guid>(type: "uuid", nullable: false),
                    LevelUpKey = table.Column<Guid>(type: "uuid", nullable: false),
                    VerticalNavItemId = table.Column<Guid>(type: "uuid", nullable: true),
                    IsDeleted = table.Column<bool>(type: "boolean", nullable: false),
                    CreatedAt = table.Column<DateTimeOffset>(type: "timestamp with time zone", nullable: false),
                    CreatedBy = table.Column<Guid>(type: "uuid", nullable: false),
                    UpdatedAt = table.Column<DateTimeOffset>(type: "timestamp with time zone", nullable: false),
                    UpdatedBy = table.Column<Guid>(type: "uuid", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_VerticalNavItems", x => x.Id);
                    table.ForeignKey(
                        name: "FK_VerticalNavItems_VerticalNavItems_VerticalNavItemId",
                        column: x => x.VerticalNavItemId,
                        principalTable: "VerticalNavItems",
                        principalColumn: "Id");
                });

            migrationBuilder.CreateTable(
                name: "AspNetRoleClaims",
                columns: table => new
                {
                    Id = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    RoleId = table.Column<string>(type: "text", nullable: false),
                    ClaimType = table.Column<string>(type: "text", nullable: true),
                    ClaimValue = table.Column<string>(type: "text", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetRoleClaims", x => x.Id);
                    table.ForeignKey(
                        name: "FK_AspNetRoleClaims_AspNetRoles_RoleId",
                        column: x => x.RoleId,
                        principalTable: "AspNetRoles",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "AspNetGroups",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uuid", nullable: false),
                    Name = table.Column<string>(type: "character varying(50)", maxLength: 50, nullable: false),
                    UniqueKey = table.Column<string>(type: "character varying(50)", maxLength: 50, nullable: false),
                    IsDeleted = table.Column<bool>(type: "boolean", nullable: false),
                    CreatedAt = table.Column<DateTimeOffset>(type: "timestamp with time zone", nullable: false),
                    CreatedBy = table.Column<Guid>(type: "uuid", nullable: false),
                    UpdatedAt = table.Column<DateTimeOffset>(type: "timestamp with time zone", nullable: false),
                    UpdatedBy = table.Column<Guid>(type: "uuid", nullable: false),
                    TenantId = table.Column<Guid>(type: "uuid", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetGroups", x => x.Id);
                    table.ForeignKey(
                        name: "FK_AspNetGroups_Tenants_TenantId",
                        column: x => x.TenantId,
                        principalTable: "Tenants",
                        principalColumn: "Id");
                });

            migrationBuilder.CreateTable(
                name: "AspNetUsers",
                columns: table => new
                {
                    UserId = table.Column<string>(type: "text", nullable: false),
                    FullName = table.Column<string>(type: "text", nullable: true),
                    Avatar = table.Column<string>(type: "text", nullable: true),
                    Setor = table.Column<int>(type: "integer", nullable: false),
                    Funcao = table.Column<int>(type: "integer", nullable: false),
                    Status = table.Column<int>(type: "integer", nullable: false),
                    Bio = table.Column<string>(type: "text", nullable: true),
                    DataAniversario = table.Column<DateTimeOffset>(type: "timestamp with time zone", nullable: false),
                    TelefoneCelular = table.Column<long>(type: "bigint", maxLength: 20, nullable: false, defaultValue: 99999999999L),
                    Genero = table.Column<int>(type: "integer", nullable: false),
                    TenantId = table.Column<Guid>(type: "uuid", nullable: true),
                    UserName = table.Column<string>(type: "character varying(256)", maxLength: 256, nullable: true),
                    NormalizedUserName = table.Column<string>(type: "character varying(256)", maxLength: 256, nullable: true),
                    Email = table.Column<string>(type: "character varying(256)", maxLength: 256, nullable: true),
                    NormalizedEmail = table.Column<string>(type: "character varying(256)", maxLength: 256, nullable: true),
                    EmailConfirmed = table.Column<bool>(type: "boolean", nullable: false),
                    PasswordHash = table.Column<string>(type: "text", nullable: true),
                    SecurityStamp = table.Column<string>(type: "text", nullable: true),
                    ConcurrencyStamp = table.Column<string>(type: "text", nullable: true),
                    PhoneNumber = table.Column<string>(type: "text", nullable: true),
                    PhoneNumberConfirmed = table.Column<bool>(type: "boolean", nullable: false),
                    TwoFactorEnabled = table.Column<bool>(type: "boolean", nullable: false),
                    LockoutEnd = table.Column<DateTimeOffset>(type: "timestamp with time zone", nullable: true),
                    LockoutEnabled = table.Column<bool>(type: "boolean", nullable: false),
                    AccessFailedCount = table.Column<int>(type: "integer", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetUsers", x => x.UserId);
                    table.ForeignKey(
                        name: "FK_AspNetUsers_Tenants_TenantId",
                        column: x => x.TenantId,
                        principalTable: "Tenants",
                        principalColumn: "Id");
                });

            migrationBuilder.CreateTable(
                name: "AspNetRoleGroups",
                columns: table => new
                {
                    RoleId = table.Column<string>(type: "text", nullable: false),
                    GroupId = table.Column<Guid>(type: "uuid", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetRoleGroups", x => new { x.RoleId, x.GroupId });
                    table.ForeignKey(
                        name: "FK_AspNetRoleGroups_AspNetGroups_GroupId",
                        column: x => x.GroupId,
                        principalTable: "AspNetGroups",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_AspNetRoleGroups_AspNetRoles_RoleId",
                        column: x => x.RoleId,
                        principalTable: "AspNetRoles",
                        principalColumn: "Id");
                });

            migrationBuilder.CreateTable(
                name: "ApplicationNotifications",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uuid", nullable: false),
                    Scope = table.Column<int>(type: "integer", nullable: false, defaultValue: 0),
                    SenderUser = table.Column<string>(type: "character varying(32)", maxLength: 32, nullable: true),
                    IsRead = table.Column<bool>(type: "boolean", nullable: false, defaultValue: false),
                    MessageTitle = table.Column<string>(type: "character varying(150)", maxLength: 150, nullable: false),
                    MessageBody = table.Column<string>(type: "character varying(255)", maxLength: 255, nullable: false),
                    MessageLabel = table.Column<string>(type: "text", nullable: true),
                    MessageLabelColor = table.Column<string>(type: "text", nullable: true),
                    MessageDate = table.Column<DateTimeOffset>(type: "timestamp with time zone", nullable: false),
                    UserId = table.Column<string>(type: "text", nullable: true),
                    IsDeleted = table.Column<bool>(type: "boolean", nullable: false),
                    CreatedAt = table.Column<DateTimeOffset>(type: "timestamp with time zone", nullable: false),
                    CreatedBy = table.Column<Guid>(type: "uuid", nullable: false),
                    UpdatedAt = table.Column<DateTimeOffset>(type: "timestamp with time zone", nullable: false),
                    UpdatedBy = table.Column<Guid>(type: "uuid", nullable: false),
                    TenantId = table.Column<Guid>(type: "uuid", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ApplicationNotifications", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ApplicationNotifications_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "UserId");
                    table.ForeignKey(
                        name: "FK_ApplicationNotifications_Tenants_TenantId",
                        column: x => x.TenantId,
                        principalTable: "Tenants",
                        principalColumn: "Id");
                });

            migrationBuilder.CreateTable(
                name: "AspNetUserClaims",
                columns: table => new
                {
                    Id = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    UserId = table.Column<string>(type: "text", nullable: false),
                    ClaimType = table.Column<string>(type: "text", nullable: true),
                    ClaimValue = table.Column<string>(type: "text", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetUserClaims", x => x.Id);
                    table.ForeignKey(
                        name: "FK_AspNetUserClaims_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "UserId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "AspNetUserGroups",
                columns: table => new
                {
                    UserId = table.Column<string>(type: "text", nullable: false),
                    GroupId = table.Column<Guid>(type: "uuid", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetUserGroups", x => new { x.UserId, x.GroupId });
                    table.ForeignKey(
                        name: "FK_AspNetUserGroups_AspNetGroups_GroupId",
                        column: x => x.GroupId,
                        principalTable: "AspNetGroups",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_AspNetUserGroups_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "UserId");
                });

            migrationBuilder.CreateTable(
                name: "AspNetUserLogins",
                columns: table => new
                {
                    LoginProvider = table.Column<string>(type: "text", nullable: false),
                    ProviderKey = table.Column<string>(type: "text", nullable: false),
                    ProviderDisplayName = table.Column<string>(type: "text", nullable: true),
                    UserId = table.Column<string>(type: "text", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetUserLogins", x => new { x.LoginProvider, x.ProviderKey });
                    table.ForeignKey(
                        name: "FK_AspNetUserLogins_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "UserId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "AspNetUserRoles",
                columns: table => new
                {
                    UserId = table.Column<string>(type: "text", nullable: false),
                    RoleId = table.Column<string>(type: "text", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetUserRoles", x => new { x.UserId, x.RoleId });
                    table.ForeignKey(
                        name: "FK_AspNetUserRoles_AspNetRoles_RoleId",
                        column: x => x.RoleId,
                        principalTable: "AspNetRoles",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_AspNetUserRoles_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "UserId");
                });

            migrationBuilder.CreateTable(
                name: "AspNetUserTokens",
                columns: table => new
                {
                    UserId = table.Column<string>(type: "text", nullable: false),
                    LoginProvider = table.Column<string>(type: "text", nullable: false),
                    Name = table.Column<string>(type: "text", nullable: false),
                    Value = table.Column<string>(type: "text", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetUserTokens", x => new { x.UserId, x.LoginProvider, x.Name });
                    table.ForeignKey(
                        name: "FK_AspNetUserTokens_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "UserId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.InsertData(
                table: "AspNetRoles",
                columns: new[] { "Id", "Actions", "ConcurrencyStamp", "Description", "Name", "NormalizedName", "Subject" },
                values: new object[,]
                {
                    { "10ac2e85-c030-48fd-815d-dcc867930263", new[] { 1 }, "d1f39a6e-7458-40b1-97fe-06c0c2b59764", "Pode listar os dados de todos os vendedores", "CanNfseList", "CANNFSELIST", "ac-nfse-page" },
                    { "2bf2e85d-bb93-4937-bebe-a0f49b1f40dc", new[] { 1 }, "38b123b8-4c32-43be-a760-5180f3ee3676", "Pode listar o título do sistema", "CanSectionTitleSystemList", "CANSECTIONTITLESYSTEMLIST", "ac-sectionTitleSystem-page" },
                    { "384a7090-00e5-4986-885e-f877eacec3c5", new[] { 1, 2, 3, 4, 5 }, "d9faef98-1d9d-4048-b510-f6323197a4ee", "Pode realizar todas as ações/operações relacionadas a entidade usuário", "CanUserAll", "CANUSERALL", "ac-user-page" },
                    { "44ccc1ab-a32d-43e6-adda-68d9df064747", new[] { 4 }, "80ffd303-5765-41f0-96e0-5d646435fb3e", "Pode atualizar os dados de um grupo", "CanGroupUpdate", "CANGROUPUPDATE", "ac-group-page" },
                    { "5be45799-9ba6-4ed8-aa86-bbaaae6a2bd2", new[] { 5 }, "e48923ce-010d-4453-83ac-09c21638fe4f", "Pode deletar um grupo", "CanGroupDelete", "CANGROUPDELETE", "ac-group-page" },
                    { "6def8621-7bd7-487d-8367-bc41c8e416fe", new[] { 3 }, "afc31cc7-4c4e-4b83-9b51-48dac3c0ada6", "Pode criar um usuário", "CanUserCreate", "CANUSERCREATE", "ac-user-page" },
                    { "7a928b4c-e679-435d-a5d0-2c44d8ed2fab", new[] { 1, 2, 3, 4, 5 }, "1d06adbc-14cc-465b-8774-a08e514d0348", "Pode realizar todas as ações/operações em todos os vendedores", "CanNfseAll", "CANNFSEALL", "ac-nfse-page" },
                    { "80ff3ca2-1c4f-484e-815d-541cfea5d5ff", new[] { 1, 2, 3, 4, 5 }, "d1c5fc92-6746-4280-a5a2-09ed4468466a", "Pode realizar todas as ações/operações em todos os grupos", "CanGroupAll", "CANGROUPALL", "ac-group-page" },
                    { "8122657c-8888-4fec-b264-0a4b6395845b", new[] { 1 }, "86a8bf16-27c7-4a63-8d7e-b5b80bbfb068", "Pode listar os dados de todos os grupos", "CanGroupList", "CANGROUPLIST", "ac-group-page" },
                    { "8e933cd2-5a98-4313-a763-24dbde13657d", new[] { 3 }, "b8018a54-35bf-4eae-9816-082988974aeb", "Pode visualizar um vendedor", "CanNfseCreate", "CANNFSECREATE", "ac-nfse-page" },
                    { "a08b8a4c-0c8f-4c8e-8953-7da2412e0155", new[] { 5 }, "f27573eb-4af3-4129-a462-f0e98e17b03f", "Pode deletar um vendedor", "CanNfseDelete", "CANNFSEDELETE", "ac-nfse-page" },
                    { "a1dd5b4a-87d1-4ac6-a372-b4b34bd6a46b", new[] { 5 }, "097829f5-549d-403b-ad7d-f9ed73cb79f5", "Pode deletar uma role/permissão", "CanRoleDelete", "CANROLEDELETE", "ac-role-page" },
                    { "a34a7359-e632-4f34-9be6-d2c5226b2c9b", new[] { 2 }, "7a447140-2531-470a-b3c8-3f8d60aad47c", "Pode listar os dados de um vendedor", "CanNfseRead", "CANNFSEREAD", "ac-nfse-page" },
                    { "a8a249bf-1a20-44b2-911e-69ab4fc4d57d", new[] { 5 }, "8d5a71b4-54c0-42dc-a94c-90af94184ec0", "Pode deletar um usuário", "CanUserDelete", "CANUSERDELETE", "ac-user-page" },
                    { "af90788d-a83d-4716-a790-f400035aac0c", new[] { 2 }, "b49a1028-9435-4fab-8b6c-974250c96b0e", "Pode listar os dado de um grupo", "CanGroupRead", "CANGROUPREAD", "ac-group-page" },
                    { "b0f96d85-3647-4651-9f78-b7529b577ec0", new[] { 0 }, "4629cea3-3b65-43b9-9c4e-7cc68fe4e4e4", "Pode realizar todas as ações/operações, bem como ter acesso a todos os dados e funcionalidades", "Master", "MASTER", "all" },
                    { "b32fb999-f533-4642-8b31-af9a810832be", new[] { 2 }, "fb1748ae-f747-428b-afb9-5d2f27173152", "Pode listar os dados de uma roles/permissão", "CanRoleRead", "CANROLEREAD", "ac-role-page" },
                    { "b3ac1477-352a-457c-848a-8b0460690440", new[] { 1, 2, 3, 4, 5 }, "57dfc5bd-2d8b-48a0-a954-a8da068485c5", "Pode realizar todas as ações/operações em todos as roles/permissões", "CanRoleAll", "CANROLEALL", "ac-role-page" },
                    { "b511691f-b279-4093-9bc1-771c5a048844", new[] { 3 }, "004dbf93-0cbd-4e86-b9ae-15dacc26780c", "Pode criar uma role/permissão", "CanRoleCreate", "CANROLECREATE", "ac-role-page" },
                    { "b94e5900-592d-4491-8460-9716b8897013", new[] { 2 }, "24d48f4e-5727-495d-9326-4bf16bc01407", "Pode listar os dados de um usuários", "CanUserRead", "CANUSERREAD", "ac-user-page" },
                    { "c72941ac-de16-4c48-b0d2-e8f7bb9d5523", new[] { 1 }, "2d37594a-e134-40f2-8038-977e9c10357f", "Pode listar o título Fiscal", "CanSectionTitleFiscalList", "CANSECTIONTITLEFISCALLIST", "ac-sectionTitleFiscal-page" },
                    { "cd324fac-8355-4742-a1b4-1e1c9080eada", new[] { 1, 2, 3, 4, 5 }, "5f0afed8-6b10-4b60-b4b2-caaeaaecbba8", "Pode realizar todas as ações/operações em dashboard publica", "CanDashboardPublicaAll", "CANDASHBOARDPUBLICAALL", "ac-dashboardPublica-page" },
                    { "d1a80f5f-6b76-4d7e-bfd0-2a6cb0b3dfc6", new[] { 4 }, "38c8b7ac-5a08-439c-9d29-e19332c848f7", "Pode atualizar os dados de uma roles/permissão", "CanRoleUpdate", "CANROLEUPDATE", "ac-role-page" },
                    { "d1f80979-48f8-4dd0-80f0-b3f7587e4a63", new[] { 1 }, "3a3f3720-46ca-4576-8f1a-ddc447d52ccc", "Pode listar os dados de todos os usuários", "CanUserList", "CANUSERLIST", "ac-user-page" },
                    { "d387ebc3-5dc1-4381-8ea6-fee6862da8f9", new[] { 4 }, "bc8892cb-d49b-4f05-8663-6448655c8680", "Pode criar um vendedor", "CanNfseUpdate", "CANNFSEUPDATE", "ac-nfse-page" },
                    { "da7100ee-e497-4155-b1dc-6b1f30beb182", new[] { 4 }, "59a12fa4-4456-4358-bb01-d230d8408d42", "Pode atualizar os dados de um usuário", "CanUserUpdate", "CANUSERUPDATE", "ac-user-page" },
                    { "df20c711-4173-47ff-929f-03a5762f98d0", new[] { 3 }, "4bbe6aa7-47c0-4672-a4fc-c41b5f452553", "Pode criar um grupo", "CanGroupCreate", "CANGROUPCREATE", "ac-group-page" },
                    { "f75ee355-ee22-4e8f-bcec-bd17df25f71a", new[] { 1 }, "41ee51ab-d1d2-4436-b3ea-4f28adb38822", "Pode listar os dados de todas as roles/permissões", "CanRoleList", "CANROLELIST", "ac-role-page" }
                });

            migrationBuilder.InsertData(
                table: "AspNetUsers",
                columns: new[] { "UserId", "AccessFailedCount", "Avatar", "Bio", "ConcurrencyStamp", "DataAniversario", "Email", "EmailConfirmed", "FullName", "Funcao", "Genero", "LockoutEnabled", "LockoutEnd", "NormalizedEmail", "NormalizedUserName", "PasswordHash", "PhoneNumber", "PhoneNumberConfirmed", "SecurityStamp", "Setor", "Status", "TenantId", "TwoFactorEnabled", "UserName" },
                values: new object[] { "8e445865-a24d-4543-a6c6-9443d048cdb9", 0, "/images/avatars/1.png", null, "ca431822-360a-4ee6-b978-66564d429fc7", new DateTimeOffset(new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), new TimeSpan(0, 0, 0, 0, 0)), "alan.rezendeeee@hotmail.com", true, "Alan Rezende", 0, 2, true, null, "ALAN.REZENDEEEE@HOTMAIL.COM", "ALAN.REZENDEEEE@HOTMAIL.COM", "AQAAAAEAACcQAAAAEFqndd0aQiH3EC2qn6j21zHj1rZKlwofwEpNhCLuA7EQsV3Fvv+S1HuJ0snxDruE8w==", null, false, "c9514850-61dd-4cc1-b909-88b79b035643", 0, 0, null, false, "alan.rezendeeee@hotmail.com" });

            migrationBuilder.InsertData(
                table: "Tenants",
                columns: new[] { "Id", "ApiKey", "CreatedAt", "CreatedBy", "EmailPrincipal", "EnderecoBairro", "EnderecoCEP", "EnderecoCidade", "EnderecoEstado", "EnderecoLogradouro", "EnderecoLogradouroNumero", "IsDeleted", "Nome", "NomeExibicao", "OficioLeituraAssinaturaCargo", "OficioLeituraAssinaturaMatricula", "OficioLeituraAssinaturaNome", "OficioLeituraVocativo1", "OficioLeituraVocativo2", "OficioLeituraVocativo3", "TelefoneDDD", "TelefoneNumero", "UpdatedAt", "UpdatedBy" },
                values: new object[] { new Guid("206c645a-2966-4ad9-19a3-dced7c201bc4"), new Guid("06b5fb02-57cb-126b-3ab2-a05f805f1e97"), new DateTimeOffset(new DateTime(2023, 5, 23, 16, 40, 44, 585, DateTimeKind.Unspecified).AddTicks(5130), new TimeSpan(0, 0, 0, 0, 0)), new Guid("8e445865-a24d-4543-a6c6-9443d048cdb9"), null, null, null, null, null, null, null, false, "Tenância Presídio Regional de Criciúma", "PRESÍDIO REGIONAL CRICIÚMA", null, null, null, null, null, null, null, null, new DateTimeOffset(new DateTime(2023, 5, 23, 16, 40, 44, 585, DateTimeKind.Unspecified).AddTicks(5250), new TimeSpan(0, 0, 0, 0, 0)), new Guid("8e445865-a24d-4543-a6c6-9443d048cdb9") });

            migrationBuilder.InsertData(
                table: "AspNetGroups",
                columns: new[] { "Id", "CreatedAt", "CreatedBy", "IsDeleted", "Name", "TenantId", "UniqueKey", "UpdatedAt", "UpdatedBy" },
                values: new object[] { new Guid("23e63d9c-283b-496b-b7d8-7dac2ef7a822"), new DateTimeOffset(new DateTime(2023, 5, 23, 16, 40, 44, 589, DateTimeKind.Unspecified).AddTicks(9550), new TimeSpan(0, 0, 0, 0, 0)), new Guid("8e445865-a24d-4543-a6c6-9443d048cdb9"), false, "Master", new Guid("206c645a-2966-4ad9-19a3-dced7c201bc4"), "ors0eAr4DPkvrwhy5gVnQAqRDnJUO43j9HzbkPyZ/7Q=", new DateTimeOffset(new DateTime(2023, 5, 23, 16, 40, 44, 589, DateTimeKind.Unspecified).AddTicks(9680), new TimeSpan(0, 0, 0, 0, 0)), new Guid("8e445865-a24d-4543-a6c6-9443d048cdb9") });

            migrationBuilder.InsertData(
                table: "AspNetUserRoles",
                columns: new[] { "RoleId", "UserId" },
                values: new object[] { "b0f96d85-3647-4651-9f78-b7529b577ec0", "8e445865-a24d-4543-a6c6-9443d048cdb9" });

            migrationBuilder.InsertData(
                table: "AspNetRoleGroups",
                columns: new[] { "GroupId", "RoleId" },
                values: new object[] { new Guid("23e63d9c-283b-496b-b7d8-7dac2ef7a822"), "b0f96d85-3647-4651-9f78-b7529b577ec0" });

            migrationBuilder.InsertData(
                table: "AspNetUserGroups",
                columns: new[] { "GroupId", "UserId" },
                values: new object[] { new Guid("23e63d9c-283b-496b-b7d8-7dac2ef7a822"), "8e445865-a24d-4543-a6c6-9443d048cdb9" });

            migrationBuilder.CreateIndex(
                name: "IX_ApplicationNotifications_TenantId",
                table: "ApplicationNotifications",
                column: "TenantId");

            migrationBuilder.CreateIndex(
                name: "IX_ApplicationNotifications_UserId",
                table: "ApplicationNotifications",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_AspNetGroups_TenantId",
                table: "AspNetGroups",
                column: "TenantId");

            migrationBuilder.CreateIndex(
                name: "IX_AspNetRoleClaims_RoleId",
                table: "AspNetRoleClaims",
                column: "RoleId");

            migrationBuilder.CreateIndex(
                name: "IX_AspNetRoleGroups_GroupId",
                table: "AspNetRoleGroups",
                column: "GroupId");

            migrationBuilder.CreateIndex(
                name: "RoleNameIndex",
                table: "AspNetRoles",
                column: "NormalizedName",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_AspNetUserClaims_UserId",
                table: "AspNetUserClaims",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_AspNetUserGroups_GroupId",
                table: "AspNetUserGroups",
                column: "GroupId");

            migrationBuilder.CreateIndex(
                name: "IX_AspNetUserLogins_UserId",
                table: "AspNetUserLogins",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_AspNetUserRoles_RoleId",
                table: "AspNetUserRoles",
                column: "RoleId");

            migrationBuilder.CreateIndex(
                name: "EmailIndex",
                table: "AspNetUsers",
                column: "NormalizedEmail");

            migrationBuilder.CreateIndex(
                name: "IX_AspNetUsers_TenantId",
                table: "AspNetUsers",
                column: "TenantId");

            migrationBuilder.CreateIndex(
                name: "UserNameIndex",
                table: "AspNetUsers",
                column: "NormalizedUserName",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_Tenants_ApiKey",
                table: "Tenants",
                column: "ApiKey",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_VerticalNavItems_VerticalNavItemId",
                table: "VerticalNavItems",
                column: "VerticalNavItemId");
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "ApplicationNotifications");

            migrationBuilder.DropTable(
                name: "AspNetRoleClaims");

            migrationBuilder.DropTable(
                name: "AspNetRoleGroups");

            migrationBuilder.DropTable(
                name: "AspNetUserClaims");

            migrationBuilder.DropTable(
                name: "AspNetUserGroups");

            migrationBuilder.DropTable(
                name: "AspNetUserLogins");

            migrationBuilder.DropTable(
                name: "AspNetUserRoles");

            migrationBuilder.DropTable(
                name: "AspNetUserTokens");

            migrationBuilder.DropTable(
                name: "VerticalNavItems");

            migrationBuilder.DropTable(
                name: "AspNetGroups");

            migrationBuilder.DropTable(
                name: "AspNetRoles");

            migrationBuilder.DropTable(
                name: "AspNetUsers");

            migrationBuilder.DropTable(
                name: "Tenants");
        }
    }
}

﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using Sagep.Application.Interfaces;
using Sagep.Application.ViewModels.RequestModels;
using System.ComponentModel.DataAnnotations;
using NetDevPack.Domain;

namespace Sagep.Api.Controllers.v1
{
    [Authorize(Roles = "Master, CanHubConnectionNfseAccess")]
    [ApiController]
    [ApiVersion("1.0")]
    [Route("api/v{version:apiVersion}/hub-connection/nfse")]
    public class HubConnectionNfseController : ApiController
    {
        private readonly ILogger<HubConnectionNfseController> _logger;
        private readonly IHubConnectionNfseAppService _hubConnectionNfseAppService;

        public HubConnectionNfseController(ILogger<HubConnectionNfseController> logger,
                                                 IHubConnectionNfseAppService hubConnectionNfseAppService)
        {
            _logger = logger;
            _hubConnectionNfseAppService = hubConnectionNfseAppService;
        }

        /// <summary>
        /// Obtém uma lista de notas fiscais do microservice sagep-nfse
        /// </summary>
        /// <param name="filters"></param>
        /// <returns>Um array json com as notas fiscais</returns>
        /// <response code="200">Lista de notas fiscais</response>
        /// <response code="400">Problemas de validação ou dados nulos</response>
        /// <response code="404">Lista vazia</response>
        /// <response code="500">Erro desconhecido</response>
        [Authorize(Roles = "Master, CanNfseList, CanNfseAll")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        [Produces("application/json")]
        [Route("list")]
        [HttpPost]
        public async Task<IActionResult> NfseListAsync([FromBody]NfseListRequestModel filters)
        {
            try 
            {            
                var result = await _hubConnectionNfseAppService.ListAsync(filters);
                _logger.LogInformation($"{result}");

                return Ok(new {
                    AllData = result.ToList(),
                    NotasFiscais = result.ToList(),
                    Params = "",
                    Total = result.Count()
                });
            }
            catch (ValidationException ex)
            {
                _logger.LogError($"Error processing route request: {ex.Message}. InnerException: {ex.InnerException?.Message}", ex);
                return BadRequest($"Error processing route request: {ex.Message}");
            }
            catch (DomainException ex)
            {
                _logger.LogError($"Error processing route request: {ex.Message}. InnerException: {ex.InnerException?.Message}", ex);
                return BadRequest($"Error processing route request: {ex.Message}");
            }
            catch (Refit.ApiException ex)
            {
                _logger.LogError($"Error calling the external microservice: {ex.Message}. InnerException: {ex.InnerException?.Message}. Response content: {ex.Content}", ex);
                return StatusCode((int)ex.StatusCode, $"Error calling the external microservice: {ex.Message}");
            }
            catch (Exception ex)
            {
                _logger.LogError($"Error processing route request: {ex.Message}. InnerException: {ex.InnerException?.Message}", ex);
                return CustomResponse(500, $"Error processing route request: {ex.Message}");
            }
        }

        /// <summary>
        /// Obtém os dados atualizados da NFS-e a partir da integração com o sistema da prefeitura
        /// </summary>
        /// <param name="notaFiscalServicoId"></param>
        /// <returns>Mensagem de sucesso caso a importação tenha ocorrido sem problemas</returns>
        /// <response code="200">NFS-e importada com sucesso</response>
        /// <response code="400">Problemas de validação ou dados nulos</response>
        /// <response code="404">NFS-e não encontrada</response>
        /// <response code="409">NFS-e já existe com os mesmos dados</response>
        /// <response code="500">Erro desconhecido</response>
        [Authorize(Roles = "Master, CanNfseRead, CanNfseAll")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status409Conflict)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        [Produces("application/json")]
        [Route("sync/{notaFiscalServicoId}")]
        [HttpGet]
        public async Task<IActionResult> SyncAsync([FromRoute]Guid notaFiscalServicoId)
        {
            #region Validations
            if (notaFiscalServicoId == Guid.Empty)
            {
                AddError("Id da NFS-e requerido.");
                return CustomResponse(200);
            }
            #endregion
            
            try
            {            
                var result = await _hubConnectionNfseAppService.SyncAsync(notaFiscalServicoId);
                _logger.LogInformation($"{result}");

                return Ok(result.Message);
            }
            catch (ValidationException ex)
            {
                _logger.LogError($"Error processing route request: {ex.Message}. InnerException: {ex.InnerException?.Message}", ex);
                return BadRequest($"Error processing route request: {ex.Message}");
            }
            catch (DomainException ex)
            {
                _logger.LogError($"Error processing route request: {ex.Message}. InnerException: {ex.InnerException?.Message}", ex);
                return BadRequest($"Error processing route request: {ex.Message}");
            }
            catch (Refit.ApiException ex)
            {
                _logger.LogError($"Error calling the external microservice: {ex.Message}. InnerException: {ex.InnerException?.Message}. Response content: {ex.Content}", ex);
                string content = ex.Content!;
                return StatusCode((int)ex.StatusCode, $"Error calling the external microservice: {content}");
            }
            catch (Exception ex)
            {
                _logger.LogError($"Error processing route request: {ex.Message}. InnerException: {ex.InnerException?.Message}", ex);
                return CustomResponse(500, $"Error processing route request: {ex.Message}");
            }
        }

        /// <summary>
        /// Envia uma NFS-e (Esta ação gera a NFS-e na api da prefeitura)
        /// </summary>
        /// <param name="id"></param>
        /// <returns>Mensagem de sucesso caso o envio tenha ocorrido com sucesso</returns>
        /// <response code="200">NFS-e enviada com sucesso</response>
        /// <response code="400">Problemas de validação ou dados nulos</response>
        /// <response code="404">NFS-e não encontrada</response>
        /// <response code="500">Erro desconhecido</response>
        [Authorize(Roles = "Master, CanNfseUpdate, CanNfseAll")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        [Produces("application/json")]
        [Route("send/{id}")]
        [HttpGet]
        public async Task<IActionResult> SendAsync([FromRoute]string id)
        {   
            try
            {            
                var result = await _hubConnectionNfseAppService.SendAsync(id);
                _logger.LogInformation($"{result}");

                return Ok(result.Message);
            }
            catch (ValidationException ex)
            {
                _logger.LogError($"Error processing route request: {ex.Message}. InnerException: {ex.InnerException?.Message}", ex);
                return BadRequest($"Error processing route request: {ex.Message}");
            }
            catch (DomainException ex)
            {
                _logger.LogError($"Error processing route request: {ex.Message}. InnerException: {ex.InnerException?.Message}", ex);
                return BadRequest($"Error processing route request: {ex.Message}");
            }
            catch (Refit.ApiException ex)
            {
                _logger.LogError($"Error calling the external microservice: {ex.Message}. InnerException: {ex.InnerException?.Message}. Response content: {ex.Content}", ex);
                string content = ex.Content!;
                return StatusCode((int)ex.StatusCode, $"Error calling the external microservice: {content}");
            }
            catch (Exception ex)
            {
                _logger.LogError($"Error processing route request: {ex.Message}. InnerException: {ex.InnerException?.Message}", ex);
                return CustomResponse(500, $"Error processing route request: {ex.Message}");
            }
        }

        /// <summary>
        /// Solicita o cancelamento de uma NFS-e (Esta ação solicita o cancelamento da NFS-e na api da prefeitura)
        /// </summary>
        /// <param name="nfseCancelRequestModel"></param>
        /// <returns>Mensagem de sucesso caso o cancelamento tenha ocorrido com sucesso</returns>
        /// <response code="200">NFS-e cancelada com sucesso</response>
        /// <response code="400">Problemas de validação ou dados nulos</response>
        /// <response code="404">NFS-e não encontrada</response>
        /// <response code="500">Erro desconhecido</response>
        [Authorize(Roles = "Master, CanNfseDelete, CanNfseAll")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        [Produces("application/json")]
        [Route("cancel")]
        [HttpPut]
        public async Task<IActionResult> CancelAsync([FromBody] NfseCancelRequestModel nfseCancelRequestModel)
        {

            try
            {            
                var result = await _hubConnectionNfseAppService.CancelAsync(nfseCancelRequestModel);
                _logger.LogInformation($"{result}");

                return Ok(result.Message);
            }
            catch (ValidationException ex)
            {
                _logger.LogError($"Error processing route request: {ex.Message}. InnerException: {ex.InnerException?.Message}", ex);
                return BadRequest($"Error processing route request: {ex.Message}");
            }
            catch (DomainException ex)
            {
                _logger.LogError($"Error processing route request: {ex.Message}. InnerException: {ex.InnerException?.Message}", ex);
                return BadRequest($"Error processing route request: {ex.Message}");
            }
            catch (Refit.ApiException ex)
            {
                _logger.LogError($"Error calling the external microservice: {ex.Message}. InnerException: {ex.InnerException?.Message}. Response content: {ex.Content}", ex);
                string content = ex.Content!;
                return StatusCode((int)ex.StatusCode, $"Error calling the external microservice: {content}");
            }
            catch (Exception ex)
            {
                _logger.LogError($"Error processing route request: {ex.Message}. InnerException: {ex.InnerException?.Message}", ex);
                return CustomResponse(500, $"Error processing route request: {ex.Message}");
            }
        }
        
        /// <summary>
        /// Solicita soft delete de uma NFS-e
        /// </summary>
        /// <param name="nfseDeleteRequestModel"></param>
        /// <returns>Mensagem de sucesso caso o excluir tenha ocorrido com sucesso</returns>
        /// <response code="200">NFS-e excluida com sucesso</response>
        /// <response code="400">Problemas de validação ou dados nulos</response>
        /// <response code="404">NFS-e não encontrada</response>
        /// <response code="500">Erro desconhecido</response>
        [Authorize(Roles = "Master, CanNfseDelete, CanNfseAll")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        [Produces("application/json")]
        [Route("changeInternalStatus")]
        [HttpPut]
        public async Task<IActionResult> DeleteAsync([FromBody] NfseDeleteRequestModel nfseDeleteRequestModel)
        {

            try
            {            
                var result = await _hubConnectionNfseAppService.DeleteAsync(nfseDeleteRequestModel);
                _logger.LogInformation($"{result}");

                return Ok(result.Message);
            }
            catch (ValidationException ex)
            {
                _logger.LogError($"Error processing route request: {ex.Message}. InnerException: {ex.InnerException?.Message}", ex);
                return BadRequest($"Error processing route request: {ex.Message}");
            }
            catch (DomainException ex)
            {
                _logger.LogError($"Error processing route request: {ex.Message}. InnerException: {ex.InnerException?.Message}", ex);
                return BadRequest($"Error processing route request: {ex.Message}");
            }
            catch (Refit.ApiException ex)
            {
                _logger.LogError($"Error calling the external microservice: {ex.Message}. InnerException: {ex.InnerException?.Message}. Response content: {ex.Content}", ex);
                string content = ex.Content!;
                return StatusCode((int)ex.StatusCode, $"Error calling the external microservice: {content}");
            }
            catch (Exception ex)
            {
                _logger.LogError($"Error processing route request: {ex.Message}. InnerException: {ex.InnerException?.Message}", ex);
                return CustomResponse(500, $"Error processing route request: {ex.Message}");
            }
        }

        /// <summary>
        /// Obtém uma nota fiscal pelo seu id a partir do microservice sagep-nfse
        /// </summary>
        /// <param name="id"></param>
        /// <returns>Um objeto json com a nota fiscal requerida</returns>
        /// <response code="200">Objeto nota fiscal</response>
        /// <response code="400">Problemas de validação ou dados nulos</response>
        /// <response code="204">Nota fiscal não encontrada</response>
        /// <response code="500">Erro desconhecido</response>
        [Authorize(Roles = "Master, CanNfseRead, CanNfseAll")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        [Produces("application/json")]
        [Route("list-one/{id}")]
        [HttpGet]
        public async Task<IActionResult> ListOneAsync([FromRoute]string id = "")
        {
            try
            {            
                var result = await _hubConnectionNfseAppService.ListOneAsync(id);
                _logger.LogInformation($"{result}");

                return Ok(new {
                    NotaFiscal = result,
                    Params = id,
                });
            }
            catch (ValidationException ex)
            {
                _logger.LogError($"Error processing route request: {ex.Message}. InnerException: {ex.InnerException?.Message}", ex);
                return BadRequest($"Error processing route request: {ex.Message}");
            }
            catch (DomainException ex)
            {
                _logger.LogError($"Error processing route request: {ex.Message}. InnerException: {ex.InnerException?.Message}", ex);
                return BadRequest($"Error processing route request: {ex.Message}");
            }
            catch (Refit.ApiException ex)
            {
                _logger.LogError($"Error calling the external microservice: {ex.Message}. InnerException: {ex.InnerException?.Message}. Response content: {ex.Content}", ex);
                return StatusCode((int)ex.StatusCode, $"Error calling the external microservice: {ex.Message}");
            }
            catch (Exception ex)
            {
                _logger.LogError($"Error processing route request: {ex.Message}. InnerException: {ex.InnerException?.Message}", ex);
                return CustomResponse(500, $"Error processing route request: {ex.Message}");
            }
        }

        /// <summary>
        /// Exporta uma lista de NFS-e para o formato XML
        /// </summary>
        /// <param name="filters"></param>
        /// <returns>Um XML único contendo todos as XML's de NFS-es</returns>
        /// <response code="200">NFS-Es exportadas com sucesso</response>
        /// <response code="400">Problemas de validação ou dados nulos</response>
        /// <response code="404">Lista vazia</response>
        /// <response code="404">Tipo de retorno não aceito</response>
        /// <response code="500">Erro desconhecido</response>
        [Authorize(Roles = "Master, CanNfseRead, CanNfseAll")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status406NotAcceptable)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        [Produces("application/xml")]
        [Route("export-xml")]
        [HttpPost]
        public async Task<IActionResult> ExportXmlAsync([FromBody]NfseListRequestModel filters)
        {
            try
            {
                var result = await _hubConnectionNfseAppService.ExportXmlAsync(filters);
                _logger.LogInformation($"{result}");

                return Ok(result);
            }
            catch (ValidationException ex)
            {
                _logger.LogError($"Error processing route request: {ex.Message}. InnerException: {ex.InnerException?.Message}", ex);
                return BadRequest($"Error processing route request: {ex.Message}");
            }
            catch (DomainException ex)
            {
                _logger.LogError($"Error processing route request: {ex.Message}. InnerException: {ex.InnerException?.Message}", ex);
                return BadRequest($"Error processing route request: {ex.Message}");
            }
            catch (Refit.ApiException ex)
            {
                _logger.LogError($"Error calling the external microservice: {ex.Message}. InnerException: {ex.InnerException?.Message}. Response content: {ex.Content}", ex);
                return StatusCode((int)ex.StatusCode, $"Error calling the external microservice: {ex.Message}");
            }
            catch (Exception ex)
            {
                _logger.LogError($"Error processing route request: {ex.Message}. InnerException: {ex.InnerException?.Message}", ex);
                return CustomResponse(500, $"Error processing route request: {ex.Message}");
            }
        }

        /// <summary>
        /// Exporta uma lista de NFS-e para o formato PDF
        /// </summary>
        /// <param name="filters"></param>
        /// <returns>Um pdf único contendo todas as NFS-es em pdf</returns>
        /// <response code="200">NFS-Es exportadas com sucesso</response>
        /// <response code="400">Problemas de validação ou dados nulos</response>
        /// <response code="404">Lista vazia</response>
        /// <response code="404">Tipo de retorno não aceito</response>
        /// <response code="500">Erro desconhecido</response>
        [Authorize(Roles = "Master, CanNfseRead, CanNfseAll")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status406NotAcceptable)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        [Produces("application/pdf")]
        [Route("export-pdf")]
        [HttpPost]
        public async Task<IActionResult> ExportPdfAsync([FromBody]NfseListRequestModel filters)
        {
            try
            {
                var pdfBytes = await _hubConnectionNfseAppService.ExportPdfAsync(filters);
                _logger.LogInformation($"{pdfBytes}");

                if (pdfBytes.IsSuccessStatusCode)
                {
                    return File(pdfBytes.Content, "application/pdf", "notas-fiscais-servico.pdf");
                }
                else
                {
                    return NotFound();
                }
                
            }
            catch (ValidationException ex)
            {
                _logger.LogError($"Error processing route request: {ex.Message}. InnerException: {ex.InnerException?.Message}", ex);
                return BadRequest($"Error processing route request: {ex.Message}");
            }
            catch (DomainException ex)
            {
                _logger.LogError($"Error processing route request: {ex.Message}. InnerException: {ex.InnerException?.Message}", ex);
                return BadRequest($"Error processing route request: {ex.Message}");
            }
            catch (Refit.ApiException ex)
            {
                _logger.LogError($"Error calling the external microservice: {ex.Message}. InnerException: {ex.InnerException?.Message}. Response content: {ex.Content}", ex);
                return StatusCode((int)ex.StatusCode, $"Error calling the external microservice: {ex.Message}");
            }
            catch (Exception ex)
            {
                _logger.LogError($"Error processing route request: {ex.Message}. InnerException: {ex.InnerException?.Message}", ex);
                return CustomResponse(500, $"Error processing route request: {ex.Message}");
            }
        }

        /// <summary>
        /// Exporta uma NFS-e para o formato PDF.
        /// </summary>
        /// <param name="id"></param>
        /// <returns>Um pdf contendo todas as informações da NFS-es.</returns>
        /// <response code="200">NFS-Es exportadas com sucesso.</response>
        /// <response code="400">Problemas de validação ou dados nulos.</response>
        /// <response code="204">NFS-e não encontrada.</response>
        /// <response code="500">Erro desconhecido</response>
        [Authorize(Roles = "Master, CanNfseRead, CanNfseAll")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status406NotAcceptable)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        [Produces("application/pdf")]
        [Route("export-one-pdf/{id}")]
        [HttpGet]
        public async Task<IActionResult> ExportOnePdfAsync([FromRoute]string id)
        {
            try
            {
                Stream pdfBytes = await _hubConnectionNfseAppService.ExportOnePdfAsync(id);
                _logger.LogInformation($"{pdfBytes}");

                return File(pdfBytes, "application/pdf", "notas-fiscais-servico.pdf");
            }
            catch (ValidationException ex)
            {
                _logger.LogError($"Error processing route request: {ex.Message}. InnerException: {ex.InnerException?.Message}", ex);
                return BadRequest($"Error processing route request: {ex.Message}");
            }
            catch (DomainException ex)
            {
                _logger.LogError($"Error processing route request: {ex.Message}. InnerException: {ex.InnerException?.Message}", ex);
                return BadRequest($"Error processing route request: {ex.Message}");
            }
            catch (Refit.ApiException ex)
            {
                _logger.LogError($"Error calling the external microservice: {ex.Message}. InnerException: {ex.InnerException?.Message}. Response content: {ex.Content}", ex);
                return StatusCode((int)ex.StatusCode, $"Error calling the external microservice: {ex.Message}");
            }
            catch (Exception ex)
            {
                _logger.LogError($"Error processing route request: {ex.Message}. InnerException: {ex.InnerException?.Message}", ex);
                return CustomResponse(500, $"Error processing route request: {ex.Message}");
            }
        }

        /// <summary>
        /// Exporta uma NFS-e para o formato XML.
        /// </summary>
        /// <param name="id"></param>
        /// <returns>Um xml contendo todas as informações da NFS-es.</returns>
        /// <response code="200">NFS-Es exportadas com sucesso.</response>
        /// <response code="400">Problemas de validação ou dados nulos.</response>
        /// <response code="204">NFS-e não encontrada.</response>
        /// <response code="500">Erro desconhecido</response>
        [Authorize(Roles = "Master, CanNfseRead, CanNfseAll")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status406NotAcceptable)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        [Produces("application/xml")]
        [Route("export-one-xml/{id}")]
        [HttpGet]
        public async Task<IActionResult> ExportOneXmlAsync([FromRoute]string id)
        {
            try
            {
                var result = await _hubConnectionNfseAppService.ExportOneXmlAsync(id);
                _logger.LogInformation($"{result}");

                return Ok(result);
            }
            catch (ValidationException ex)
            {
                _logger.LogError($"Error processing route request: {ex.Message}. InnerException: {ex.InnerException?.Message}", ex);
                return BadRequest($"Error processing route request: {ex.Message}");
            }
            catch (DomainException ex)
            {
                _logger.LogError($"Error processing route request: {ex.Message}. InnerException: {ex.InnerException?.Message}", ex);
                return BadRequest($"Error processing route request: {ex.Message}");
            }
            catch (Refit.ApiException ex)
            {
                _logger.LogError($"Error calling the external microservice: {ex.Message}. InnerException: {ex.InnerException?.Message}. Response content: {ex.Content}", ex);
                return StatusCode((int)ex.StatusCode, $"Error calling the external microservice: {ex.Message}");
            }
            catch (Exception ex)
            {
                _logger.LogError($"Error processing route request: {ex.Message}. InnerException: {ex.InnerException?.Message}", ex);
                return CustomResponse(500, $"Error processing route request: {ex.Message}");
            }
        }

        /// <summary>
        /// Envia uma NFS-e via e-mail.
        /// </summary>
        /// <param name="filters"></param>
        /// <returns>Um objeto de resposta padrão com as propriedades statusCode e message</returns>
        /// <response code="200">E-mail enviado com sucesso.</response>
        /// <response code="400">Problemas de validação ou dados nulos.</response>
        /// <response code="204">NFS-e não encontrada.</response>
        /// <response code="500">Erro desconhecido</response>
        [Authorize(Roles = "Master, CanNfseRead, CanNfseAll")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status406NotAcceptable)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        [Produces("application/json")]
        [Route("send-email")]
        [HttpPost]
        public async Task<IActionResult> SendEmailAsync([FromBody]NfseSendEmailRequestModel filters)
        {
            try
            {
                var result = await _hubConnectionNfseAppService.SendEmailAsync(filters);
                _logger.LogInformation($"{result}");

                return Ok(result);
            }
            catch (ValidationException ex)
            {
                _logger.LogError($"Error processing route request: {ex.Message}. InnerException: {ex.InnerException?.Message}", ex);
                return BadRequest($"Error processing route request: {ex.Message}");
            }
            catch (DomainException ex)
            {
                _logger.LogError($"Error processing route request: {ex.Message}. InnerException: {ex.InnerException?.Message}", ex);
                return BadRequest($"Error processing route request: {ex.Message}");
            }
            catch (Refit.ApiException ex)
            {
                _logger.LogError($"Error calling the external microservice: {ex.Message}. InnerException: {ex.InnerException?.Message}. Response content: {ex.Content}", ex);
                return StatusCode((int)ex.StatusCode, $"Error calling the external microservice: {ex.Message}");
            }
            catch (Exception ex)
            {
                _logger.LogError($"Error processing route request: {ex.Message}. InnerException: {ex.InnerException?.Message}", ex);
                return CustomResponse(500, $"Error processing route request: {ex.Message}");
            }
        }

        
        /// <summary>
        /// Obtém dados para o dashboard de quantidade de notas por status
        /// </summary>
        /// <returns>Um objeto json total e total por status</returns>
        /// <response code="200">Objeto quantidade de notas</response>
        /// <response code="400">Problemas de validação ou dados nulos</response>
        /// <response code="204">Nada encontrado</response>
        /// <response code="500">Erro desconhecido</response>
        [Authorize(Roles = "Master, CanNfseRead, CanNfseAll")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        [Produces("application/json")]
        [Route("dashboard")]
        [HttpGet]
        public async Task<IActionResult> DashboardAsync()
        {
            try
            {            
                var result = await _hubConnectionNfseAppService.DashboardAsync();
                _logger.LogInformation($"{result}");

                return Ok(result);
            }
            catch (ValidationException ex)
            {
                _logger.LogError($"Error processing route request: {ex.Message}. InnerException: {ex.InnerException?.Message}", ex);
                return BadRequest($"Error processing route request: {ex.Message}");
            }
            catch (DomainException ex)
            {
                _logger.LogError($"Error processing route request: {ex.Message}. InnerException: {ex.InnerException?.Message}", ex);
                return BadRequest($"Error processing route request: {ex.Message}");
            }
            catch (Refit.ApiException ex)
            {
                _logger.LogError($"Error calling the external microservice: {ex.Message}. InnerException: {ex.InnerException?.Message}. Response content: {ex.Content}", ex);
                return StatusCode((int)ex.StatusCode, $"Error calling the external microservice: {ex.Message}");
            }
            catch (Exception ex)
            {
                _logger.LogError($"Error processing route request: {ex.Message}. InnerException: {ex.InnerException?.Message}", ex);
                return CustomResponse(500, $"Error processing route request: {ex.Message}");
            }
        }
        
        /// <summary>
        /// Obtém lista de configurações rps
        /// </summary>
        /// <returns>Um objeto json total e total por status</returns>
        /// <response code="200">Objeto quantidade de notas</response>
        /// <response code="400">Problemas de validação ou dados nulos</response>
        /// <response code="204">Nada encontrado</response>
        /// <response code="500">Erro desconhecido</response>
        [Authorize(Roles = "Master, CanNfseRead, CanNfseAll")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        [Produces("application/json")]
        [Route("config")]
        [HttpGet]
        public async Task<IActionResult> ConfigAsync()
        {
            try
            {            
                var result = await _hubConnectionNfseAppService.ConfigAsync();
                _logger.LogInformation($"{result}");

                return Ok(result);
            }
            catch (ValidationException ex)
            {
                _logger.LogError($"Error processing route request: {ex.Message}. InnerException: {ex.InnerException?.Message}", ex);
                return BadRequest($"Error processing route request: {ex.Message}");
            }
            catch (DomainException ex)
            {
                _logger.LogError($"Error processing route request: {ex.Message}. InnerException: {ex.InnerException?.Message}", ex);
                return BadRequest($"Error processing route request: {ex.Message}");
            }
            catch (Refit.ApiException ex)
            {
                _logger.LogError($"Error calling the external microservice: {ex.Message}. InnerException: {ex.InnerException?.Message}. Response content: {ex.Content}", ex);
                return StatusCode((int)ex.StatusCode, $"Error calling the external microservice: {ex.Message}");
            }
            catch (Exception ex)
            {
                _logger.LogError($"Error processing route request: {ex.Message}. InnerException: {ex.InnerException?.Message}", ex);
                return CustomResponse(500, $"Error processing route request: {ex.Message}");
            }
        }
        
        /// <summary>
        /// Adiciona registro de configurações rps
        /// </summary>
        /// <returns>Um objeto json total e total por status</returns>
        /// <response code="200">Objeto quantidade de notas</response>
        /// <response code="400">Problemas de validação ou dados nulos</response>
        /// <response code="204">Nada encontrado</response>
        /// <response code="500">Erro desconhecido</response>
        [Authorize(Roles = "Master, CanNfseRead, CanNfseAll")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        [Produces("application/json")]
        [Route("config")]
        [HttpPost]
        public async Task<IActionResult> ConfigAddAsync([FromBody]NfseConfigRequestModelAdd dataConfig)
        {
            try
            {            
                var result = await _hubConnectionNfseAppService.ConfigAddAsync(dataConfig);
                _logger.LogInformation($"{result}");

                return Ok(result);
            }
            catch (ValidationException ex)
            {
                _logger.LogError($"Error processing route request: {ex.Message}. InnerException: {ex.InnerException?.Message}", ex);
                return BadRequest($"Error processing route request: {ex.Message}");
            }
            catch (DomainException ex)
            {
                _logger.LogError($"Error processing route request: {ex.Message}. InnerException: {ex.InnerException?.Message}", ex);
                return BadRequest($"Error processing route request: {ex.Message}");
            }
            catch (Refit.ApiException ex)
            {
                _logger.LogError($"Error calling the external microservice: {ex.Message}. InnerException: {ex.InnerException?.Message}. Response content: {ex.Content}", ex);
                string content = ex.Content!;
                return StatusCode((int)ex.StatusCode, $"Error calling the external microservice: {content}");
            }
            catch (Exception ex)
            {
                _logger.LogError($"Error processing route request: {ex.Message}. InnerException: {ex.InnerException?.Message}", ex);
                return CustomResponse(500, $"Error processing route request: {ex.Message}");
            }
        }
        
        /// <summary>
        /// Atualiza registro de configurações rps
        /// </summary>
        /// <returns>Um objeto json total e total por status</returns>
        /// <response code="200">Objeto quantidade de notas</response>
        /// <response code="400">Problemas de validação ou dados nulos</response>
        /// <response code="204">Nada encontrado</response>
        /// <response code="500">Erro desconhecido</response>
        [Authorize(Roles = "Master, CanNfseRead, CanNfseAll")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        [Produces("application/json")]
        [Route("config")]
        [HttpPut]
        public async Task<IActionResult> ConfigUpdateAsync([FromBody]NfseConfigRequestModelUpdate dataConfig)
        {
            try
            {            
                var result = await _hubConnectionNfseAppService.ConfigUpdateAsync(dataConfig);
                _logger.LogInformation($"{result}");

                return Ok(result);
            }
            catch (ValidationException ex)
            {
                _logger.LogError($"Error processing route request: {ex.Message}. InnerException: {ex.InnerException?.Message}", ex);
                return BadRequest($"Error processing route request: {ex.Message}");
            }
            catch (DomainException ex)
            {
                _logger.LogError($"Error processing route request: {ex.Message}. InnerException: {ex.InnerException?.Message}", ex);
                return BadRequest($"Error processing route request: {ex.Message}");
            }
            catch (Refit.ApiException ex)
            {
                _logger.LogError($"Error calling the external microservice: {ex.Message}. InnerException: {ex.InnerException?.Message}. Response content: {ex.Content}", ex);
                string content = ex.Content!;
                return StatusCode((int)ex.StatusCode, $"Error calling the external microservice: {content}");
            }
            catch (Exception ex)
            {
                _logger.LogError($"Error processing route request: {ex.Message}. InnerException: {ex.InnerException?.Message}", ex);
                return CustomResponse(500, $"Error processing route request: {ex.Message}");
            }
        }
        
        /// <summary>
        /// Exclui registro de configurações rps
        /// </summary>
        /// <returns>Um objeto json total e total por status</returns>
        /// <response code="200">Objeto quantidade de notas</response>
        /// <response code="400">Problemas de validação ou dados nulos</response>
        /// <response code="204">Nada encontrado</response>
        /// <response code="500">Erro desconhecido</response>
        [Authorize(Roles = "Master, CanNfseRead, CanNfseAll")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        [Produces("application/json")]
        [Route("config/{id}")]
        [HttpDelete]
        public async Task<IActionResult> ConfigDeleteAsync([FromRoute]string id)
        {
            try
            {            
                var result = await _hubConnectionNfseAppService.ConfigDeleteAsync(id);
                _logger.LogInformation($"{result}");

                return Ok(result);
            }
            catch (ValidationException ex)
            {
                _logger.LogError($"Error processing route request: {ex.Message}. InnerException: {ex.InnerException?.Message}", ex);
                return BadRequest($"Error processing route request: {ex.Message}");
            }
            catch (DomainException ex)
            {
                _logger.LogError($"Error processing route request: {ex.Message}. InnerException: {ex.InnerException?.Message}", ex);
                return BadRequest($"Error processing route request: {ex.Message}");
            }
            catch (Refit.ApiException ex)
            {
                _logger.LogError($"Error calling the external microservice: {ex.Message}. InnerException: {ex.InnerException?.Message}. Response content: {ex.Content}", ex);
                string content = ex.Content!;
                return StatusCode((int)ex.StatusCode, $"Error calling the external microservice: {content}");
            }
            catch (Exception ex)
            {
                _logger.LogError($"Error processing route request: {ex.Message}. InnerException: {ex.InnerException?.Message}", ex);
                return CustomResponse(500, $"Error processing route request: {ex.Message}");
            }
        }
        
        /// <summary>
        /// Obtém aviso configurações rps
        /// </summary>
        /// <returns>string</returns>
        /// <response code="200">Mensagem</response>
        /// <response code="400">Problemas de validação ou dados nulos</response>
        /// <response code="204">Nada encontrado</response>
        /// <response code="500">Erro desconhecido</response>
        [Authorize(Roles = "Master, CanNfseRead, CanNfseAll")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        [Route("config/warning")]
        [HttpGet]
        public async Task<IActionResult> ConfigWarningAsync()
        {
            try
            {            
                var result = await _hubConnectionNfseAppService.ConfigWarningAsync();
                _logger.LogInformation($"{result}");

                return Ok(result);
            }
            catch (ValidationException ex)
            {
                _logger.LogError($"Error processing route request: {ex.Message}. InnerException: {ex.InnerException?.Message}", ex);
                return BadRequest($"Error processing route request: {ex.Message}");
            }
            catch (DomainException ex)
            {
                _logger.LogError($"Error processing route request: {ex.Message}. InnerException: {ex.InnerException?.Message}", ex);
                return BadRequest($"Error processing route request: {ex.Message}");
            }
            catch (Refit.ApiException ex)
            {
                _logger.LogError($"Error calling the external microservice: {ex.Message}. InnerException: {ex.InnerException?.Message}. Response content: {ex.Content}", ex);
                return StatusCode((int)ex.StatusCode, $"Error calling the external microservice: {ex.Message}");
            }
            catch (Exception ex)
            {
                _logger.LogError($"Error processing route request: {ex.Message}. InnerException: {ex.InnerException?.Message}", ex);
                return CustomResponse(500, $"Error processing route request: {ex.Message}");
            }
        }
    }
}
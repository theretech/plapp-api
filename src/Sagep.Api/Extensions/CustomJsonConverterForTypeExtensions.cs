using Newtonsoft.Json;
using System;
using System.Diagnostics.CodeAnalysis;
using System.IO;

public class CustomJsonConverterForTypeExtensionsNewtonsoft : JsonConverter<Type>
{
    public override Type ReadJson(JsonReader reader, Type objectType, [AllowNull] Type existingValue, bool hasExistingValue, JsonSerializer serializer)
    {
        // Caution: Deserialization of type instances like this 
        // is not recommended and should be avoided
        // since it can lead to potential security issues.

        // If you really want this supported (for instance if the JSON input is trusted):
        // string assemblyQualifiedName = reader.Value?.ToString();
        // return Type.GetType(assemblyQualifiedName);
        throw new NotSupportedException();
    }

    public override void WriteJson(JsonWriter writer, [AllowNull] Type value, JsonSerializer serializer)
    {
        string assemblyQualifiedName = value?.AssemblyQualifiedName ?? string.Empty;
        // Use this with caution, since you are disclosing type information.
        writer.WriteValue(assemblyQualifiedName);
    }
}
namespace Sagep.Api.ProgramConfigurations
{
    public static class CorsConfiguration
    {
        public static void AddCorsConfiguration(this IServiceCollection services)
        {
            if (services == null) throw new ArgumentNullException(nameof(services));

            services.AddCors(options =>
            {
                options.AddPolicy("DevelopmentPermission",
                    builder => builder
                                .WithOrigins("https://app.sagep.com.br",
                                             "http://10.10.10.5",
                                             "http://localhost:3000",
                                             "http://189.45.204.190:8080")
                                .AllowAnyHeader()
                                .AllowAnyMethod());
            });
        }
    }
}
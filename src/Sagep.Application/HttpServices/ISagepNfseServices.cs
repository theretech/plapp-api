using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using Refit;
using Sagep.Application.ViewModels.RequestModels;
using Sagep.Application.ViewModels.ResponseModel;

namespace Sagep.Application.HttpServices
{
    public interface ISagepNfseServices
    {
        /// <summary>
        /// Obtém uma lista de notas fiscais do microservice sagep-nfse
        /// </summary>
        /// <param name="apiKey"></param>
        /// <param name="filters"></param>
        /// <returns>Retorna um array json com uma lista de notas fiscais.</returns>
        /// <response code="200">Retorna sucesso com um array json com as notas fiscais.</response>
        /// <response code="404">Nenhuma nota fiscal encontrada.</response>
        /// <response code="400">Problemas de validação ou dados nulos</response>
        /// <response code="500">Erro interno desconhecido</response>
        [Post("/list")]
        [Headers("Content-Type: application/json")]
        Task<IList<NfseListResponseModel>> ListAsync([Header("Authorization")] string apiKey, NfseListRequestModel filters);

        /// <summary>
        /// Obtém uma NFS-e com base no seu id no microservice sagep-nfse
        /// </summary>
        /// <param name="apiKey"></param>
        /// <param name="id"></param>
        /// <returns>Retorna um array json com uma lista de notas fiscais.</returns>
        /// <response code="200">Retorna sucesso com um array json com as notas fiscais.</response>
        /// <response code="204">NFS-e não encontrada.</response>
        /// <response code="400">Problemas de validação ou dados nulos</response>
        /// <response code="500">Erro interno desconhecido</response>
        [Get("/viewNfseSelected/{id}")]
        [Headers("Content-Type: application/json")]
        Task<NfseListResponseModel> ListOneAsync([Header("Authorization")] string apiKey, string id);

        /// <summary>
        /// Efetuar o import das notas fiscais.
        /// </summary>
        /// <param name="apiKey"></param>
        /// <param name="id"></param>
        /// <returns>Retorna um objeto padrão de retorno com uma propriedade message e uma propriedade statuscode.</returns>
        /// <response code="200">NFS-e importada com sucesso.</response>
        /// <response code="204">NFS-e não encontrada.</response>
        /// <response code="400">Problemas de validação ou dados nulos</response>
        /// <response code="500">Erro interno desconhecido</response>
        [Get("/consult")]
        Task<ResponseBaseModel> SyncAsync([Header("Authorization")] string apiKey, string id);

        /// <summary>
        /// Envia a NFS-e para o microservice sagep-nfs-e enviar para a api da prefeitura (Geração a NFS-e).
        /// </summary>
        /// <param name="apiKey"></param>
        /// <param name="id"></param> 
        /// <returns>Retorna um objeto padrão de retorno com uma propriedade message e uma propriedade statuscode.</returns>
        /// <response code="200">NFS-e enviada com sucesso.</response>
        /// <response code="400">Problemas de validação ou dados nulos.</response>
        /// <response code="500">Erro interno desconhecido.</response>
        [Get("/generate/{id}")]
        Task<ResponseBaseModel> SendAsync([Header("Authorization")] string apiKey, string id);

        /// <summary>
        /// Solicita o cancelamento da NFS-e para o microservice sagep-nfs-e que o fará para a api da prefeitura (Geração a NFS-e).
        /// </summary>
        /// <param name="apiKey"></param>
        /// <param name="nfseCancelRequestModel"></param>
        /// <returns>Retorna um objeto padrão de retorno com uma propriedade message e uma propriedade statuscode.</returns>
        /// <response code="200">NFS-e cancelada com sucesso.</response>
        /// <response code="204">NFS-e não encontrada.</response>
        /// <response code="400">Problemas de validação ou dados nulos.</response>
        /// <response code="500">Erro interno desconhecido.</response>
        [Put("/cancel")]
        Task<ResponseBaseModel> CancelAsync([Header("Authorization")] string apiKey, NfseCancelRequestModel nfseCancelRequestModel);

        /// <summary>
        /// soft delete da NFS-e para o microservice sagep-nfs-e.
        /// </summary>
        /// <param name="apiKey"></param>
        /// <param name="nfseDeleteRequestModel"></param>
        /// <returns>ok caso excluido com sucesso</returns>
        /// <response code="200">NFS-e excluida com sucesso.</response>
        /// <response code="204">NFS-e não encontrada.</response>
        /// <response code="400">Problemas de validação ou dados nulos.</response>
        /// <response code="500">Erro interno desconhecido.</response>
        [Put("/changeInternalStatus")]
        Task<ResponseBaseModel> DeleteAsync([Header("Authorization")] string apiKey, NfseDeleteRequestModel nfseDeleteRequestModel);

        /// <summary>
        /// Exporta uma ou diversas NFS-es para o formato XML
        /// </summary>
        /// <param name="apiKey"></param>
        /// <param name="filters"></param>
        /// <returns>Retorna uma string contendo o xml de uma ou diversas NFS-es.</returns>
        /// <response code="200">NFS-Es exportadas com sucesso.</response>
        /// <response code="204">NFS-e não encontrada.</response>
        /// <response code="400">Problemas de validação ou dados nulos</response>
        /// <response code="500">Erro interno desconhecido</response>
        [Post("/export")]
        [Headers("Content-Type: application/json")]
        Task<Stream> ExportXmlAsync([Header("Authorization")] string apiKey, NfseListRequestModel filters);

        /// <summary>
        /// Exporta uma ou diversas NFS-es para o formato PDF.
        /// </summary>
        /// <param name="apiKey"></param>
        /// <param name="filters"></param>
        /// <returns>Retorna um byte array contendo as notas fiscais em formato pdf.</returns>
        /// <response code="200">NFS-Es exportadas com sucesso.</response>
        /// <response code="404">NFS-es não encontradas para exportação.</response>
        /// <response code="400">Problemas de validação ou dados nulos</response>
        /// <response code="500">Erro interno desconhecido</response>
        [Post("/export")]
        [Headers("Content-Type: application/json")]
        Task<ApiResponse<Stream>> ExportPdfAsync([Header("Authorization")] string apiKey, NfseListRequestModel filters);

        /// <summary>
        /// Exporta uma NFS-es para o formato PDF.
        /// </summary>
        /// <param name="apiKey"></param>
        /// <param name="id"></param>
        /// <param name="exportType"></param>
        /// <returns>Retorna um byte array contendo as notas fiscais em formato pdf.</returns>
        /// <response code="200">NFS-Es exportadas com sucesso.</response>
        /// <response code="204">NFS-es não encontradas para exportação.</response>
        /// <response code="400">Problemas de validação ou dados nulos.</response>
        /// <response code="500">Erro interno desconhecido.</response>
        [Get("/exportOne")]
        [Headers("Content-Type: application/pdf")]
        Task<Stream> ExportOnePdfAsync([Header("Authorization")] string apiKey, string id, string exportType);

        /// <summary>
        /// Exporta uma NFS-es para o formato XML.
        /// </summary>
        /// <param name="apiKey"></param>
        /// <param name="id"></param>
        /// <param name="exportType"></param>
        /// <returns>Retorna uma string contendo uma nota fiscal no formato xml.</returns>
        /// <response code="200">NFS-e exportada com sucesso.</response>
        /// <response code="204">NFS-e não encontradas para exportação.</response>
        /// <response code="400">Problemas de validação ou dados nulos.</response>
        /// <response code="500">Erro interno desconhecido.</response>
        [Get("/exportOne")]
        [Headers("Content-Type: application/xml")]
        Task<Stream> ExportOneXmlAsync([Header("Authorization")] string apiKey, string id, string exportType);

        /// <summary>
        /// Envia e-mail com a NFS-e anexa.
        /// </summary>
        /// <param name="apiKey"></param>
        /// <param name="filters"></param>
        /// <returns>Retorna um objeto padrão com um statusCode e message.</returns>
        /// <response code="200">NFS-e exportada com sucesso.</response>
        /// <response code="204">NFS-e não encontradas para exportação.</response>
        /// <response code="400">Problemas de validação ou dados nulos.</response>
        /// <response code="500">Erro interno desconhecido.</response>
        [Post("/sendEmail")]
        [Headers("Content-Type: application/json")]
        Task<ResponseBaseModel> SendEmailAsync([Header("Authorization")] string apiKey, NfseSendEmailRequestModel filters);
        
        /// <summary>
        /// Obtém dados para o dashboard de quantidade de notas por status
        /// </summary>
        /// <param name="apiKey"></param>
        /// <returns>Um objeto json total e total por status</returns>
        /// <response code="200">Objeto quantidade de notas</response>
        /// <response code="400">Problemas de validação ou dados nulos</response>
        /// <response code="204">Nada encontrado</response>
        /// <response code="500">Erro desconhecido</response>
        [Get("/dashboard")]
        Task<NfseDashResponseModel> DashboardAsync([Header("Authorization")] string apiKey);

        /// <summary>
        /// Obtém lista de configurações rps
        /// </summary>
        /// <param name="apiKey"></param>
        /// <returns>Um objeto json total e total por status</returns>
        /// <response code="200">Objeto quantidade de notas</response>
        /// <response code="400">Problemas de validação ou dados nulos</response>
        /// <response code="204">Nada encontrado</response>
        /// <response code="500">Erro desconhecido</response>
        [Get("/config")]
        Task<IList<NfseConfigResponseModel>> ConfigAsync([Header("Authorization")] string apiKey);
        
        /// <summary>
        /// Cria um registro de configurações rps
        /// </summary>
        /// <param name="apiKey"></param>
        /// <param name="dataConfig"></param>
        /// <returns>ok caso criado com sucesso</returns>
        /// <response code="200">Objeto quantidade de notas</response>
        /// <response code="400">Problemas de validação ou dados nulos</response>
        /// <response code="204">Nada encontrado</response>
        /// <response code="500">Erro desconhecido</response>
        [Post("/config")]
        Task<ResponseBaseModel> ConfigAddAsync([Header("Authorization")] string apiKey, NfseConfigRequestModelAdd dataConfig);
        
        /// <summary>
        /// Atualiza um registro de configurações rps
        /// </summary>
        /// <param name="apiKey"></param>
        /// <param name="dataConfig"></param>
        /// <returns>ok caso atualizado com sucesso</returns>
        /// <response code="200">Objeto quantidade de notas</response>
        /// <response code="400">Problemas de validação ou dados nulos</response>
        /// <response code="204">Nada encontrado</response>
        /// <response code="500">Erro desconhecido</response>
        [Put("/config")]
        Task<ResponseBaseModel> ConfigUpdateAsync([Header("Authorization")] string apiKey, NfseConfigRequestModelUpdate dataConfig);
        
        /// <summary>
        /// Exclui um registro de configurações rps
        /// </summary>
        /// <param name="apiKey"></param>
        /// <param name="id"></param>
        /// <returns>ok caso excluido com sucesso</returns>
        /// <response code="200">Objeto quantidade de notas</response>
        /// <response code="400">Problemas de validação ou dados nulos</response>
        /// <response code="204">Nada encontrado</response>
        /// <response code="500">Erro desconhecido</response>
        [Delete("/config/{id}")]
        Task<ResponseBaseModel> ConfigDeleteAsync([Header("Authorization")] string apiKey, string id);
        
        /// <summary>
        /// Exclui um registro de configurações rps
        /// </summary>
        /// <param name="apiKey"></param>
        /// <returns>string</returns>
        /// <response code="200">Mensagem</response>
        /// <response code="400">Problemas de validação ou dados nulos</response>
        /// <response code="204">Nada encontrado</response>
        /// <response code="500">Erro desconhecido</response>
        [Get("/config/warning")]
        Task<string> ConfigWarningAsync([Header("Authorization")] string apiKey);
    }
}
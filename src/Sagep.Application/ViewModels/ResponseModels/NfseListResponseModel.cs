using System;
using System.Collections.Generic;

namespace Sagep.Application.ViewModels.ResponseModel
{
    public class NfseListResponseModel
    {
        public string Id { get; set; }
        public int? NumeroNotaFiscal { get; set; } = null;
        public int? Sequencial { get; set; }
        public TenantViewModel Tenant { get; set; }
        public TomadorViewModel Tomador { get; set; }
        public Int32? NotaFiscalStatusId { get; set; }
        public string CodigoChamada { get; set; }
        public Int32? CodigoEmpresa { get; set; }
        public string MesChamada { get; set; }
        public Int32? CodigoConvenio { get; set; }

#pragma warning disable CS8632
        public string? InformacoesComplementares { get; set;  }
        public int? CodigoCancelamento { get; set; }
        public string? DataGeracao { get; set; }
        public string? Competencia { get; set; }
#pragma warning disable CS8632

        public ICollection<NotaFiscalServicoViewModel> Servicos { get; set; }
    }
}
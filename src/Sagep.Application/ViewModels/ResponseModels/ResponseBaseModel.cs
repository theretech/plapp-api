using System;

namespace Sagep.Application.ViewModels.ResponseModel
{
    public class ResponseBaseModel
    {
        public string Message { get; set; }
        public Int32 StatusCode { get; set; }
    }
}
using System.Collections.Generic;

namespace Sagep.Application.ViewModels.ResponseModel
{
    public class NfseDashResponseModel
    {
        public int Total { get; set; }
#pragma warning disable CS8632

        public ICollection<int> Status { get; set; }
    }
}
using System.Xml.Serialization;

namespace Sagep.Application.ViewModels.ResponseModel
{
    [XmlRoot(ElementName = "NfseExportResponse", Namespace = "http://www.abrasf.org.br/nfse.xsd")]
    public class NfseExportResponseModel
    {
        public string Xml { get; set; }
    }
}
using System;

namespace Sagep.Application.ViewModels.ResponseModel
{
    public class NfseConfigResponseModel
    {
        public string Id { get; set; }
        public DateTimeOffset DataCadastro { get; set; }
        public bool IsAtivo { get; set; }
        public bool IsExpirado { get; set; }
        public int RpsInicial { get; set; }
        public int RpsFinal { get; set; }
        public int? RpsAtual { get; set; }
        public int RpsAviso { get; set; }
    }
}
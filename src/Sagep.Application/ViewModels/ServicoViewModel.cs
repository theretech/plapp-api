using System;
using System.ComponentModel;
namespace Sagep.Application.ViewModels
{
    public class ServicoViewModel
    {
        public string Id { get; set; }
        public Int32 CodigoCnae { get; set; }
        public string CodigoTributacaoMunicipio { get; set; }
        public string CodigoNbs { get; set; }
        public Int32 CodigoMunicipio { get; set; }
        public Int32 ExigibilidadeISS { get; set; }
        public string IdentificacaoNaoExibilidade { get; set; }
        public Int32 MunicipioIncidencia { get; set; }
    }
}
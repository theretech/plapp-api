using System;

namespace Sagep.Application.ViewModels
{
    public class NotaFiscalServicoViewModel
    {
        public string Id { get; set; }
        public string ServicoId { get; set; }
        public string NotaFiscalId { get; set; }
        public string ValorServico { get; set; }
        public bool IsIssRetido { get; set; } = false;
        public Int32? ResponsavelRetencao { get; set; }
        public string ItemListaServico { get; set; }
        public string Discriminacao { get; set; }
        public Int32? CodigoCnae { get; set; }
        public string CodigoTributacaoMunicipio { get; set; }
        public Int32? CodigoMunicipio { get; set; }
        public Int32? ExigibilidadeISS { get; set; }
        public Int32? MunicipioIncidencia { get; set; }

#pragma warning disable CS8632
        public string? ValorDeducoes { get; set; }
        public string? ValorPis { get; set; }
        public string? ValorCofins { get; set; }
        public string? ValorInss { get; set; }
        public string? ValorIr { get; set; }
        public string? ValorCsll { get; set; }
        public string? OutrasRetencoes { get; set; }
        public string? ValorTotalTributos { get; set; }
        public string? ValorIss { get; set; }
        public string? Aliquota { get; set; }
        public string? DescontoIncondicionado { get; set; }
        public string? DescontoCondicionado { get; set; }
        public string? NumeroProcesso { get; set; }
        public string? CodigoNbs { get; set; }
        public string? IdentificacaoNaoExigibilidade { get; set; }
        public string? TenantId { get; set; }
#pragma warning disable CS8632
    }
}
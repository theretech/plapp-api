using System;

namespace Sagep.Application.ViewModels
{
    public class TomadorViewModel
    {
        public string RazaoSocial { get; set; }
        public Int32? CodigoMunicipio { get; set; }

#pragma warning disable CS8632
        public string? Id { get; set; }
        public string? Bairro { get; set; }
        public string? Uf { get; set; }
        public string? Cep { get; set; }
        public string? Endereco { get; set; }
        public string? Numero { get; set; }
        public string? Cnpj { get; set; }
        public string? Cpf { get; set; }
        public string? InscricaoMunicipal { get; set; }
        public string? Complemento { get; set; }
        public string? Telefone { get; set; }
        public string? Email { get; set; }
#pragma warning restore CS8632
    }
}
using System;

namespace Sagep.Application.ViewModels.RequestModels
{
    public class NfseListRequestModel
    {
        public string CompetenciaFrom { get; set; }
        public string CompetenciaTo { get; set; }
        public string DataGeracaoFrom { get; set; }
        public string DataGeracaoTo { get; set; }
        public string CnpjTomador { get; set; }
        public string NomeTomador { get; set; }
        public Int32? NotaFiscalStatusId { get; set; }
        public Int32? NumeroNotaFiscal { get; set; }
        public string MesChamada { get; set; }
        public Int32? CodigoEmpresa { get; set; }
        public Int32? CodigoConvenio { get; set; }
        public Int32? ValorServicoFrom { get; set;}
        public Int32? ValorServicoTo { get; set;}
        public Int32? Sequencial { get; set;}
        public string ExportType { get; set; }
    }
}
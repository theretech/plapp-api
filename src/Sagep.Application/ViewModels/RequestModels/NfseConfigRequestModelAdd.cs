using System.ComponentModel.DataAnnotations;

namespace Sagep.Application.ViewModels.RequestModels
{
    public class NfseConfigRequestModelAdd
    {
        [Required(ErrorMessage = "\nRPS inicial requerido.")]
        public int RpsInicial { get; set; }

        [Required(ErrorMessage = "\nRPS final requerido.")]
        public int RpsFinal { get; set; }
    }
}
namespace Sagep.Application.ViewModels.RequestModels
{
    public class NfseSendEmailRequestModel
    {
        public string Id { get; set; }
        public string Email { get; set; }
    }
}
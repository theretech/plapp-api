using System;
using System.ComponentModel.DataAnnotations;

namespace Sagep.Application.ViewModels.RequestModels
{
    public class NfseConfigRequestModelUpdate
    {
        [Required(ErrorMessage = "\nId requerido.")]
        public string Id { get; set; }
        public bool? IsAtivo { get; set; }
        public bool? IsExpirado { get; set; }

        [Required(ErrorMessage = "\nRPS inicial requerido.")]
        public int RpsInicial { get; set; }

        [Required(ErrorMessage = "\nRPS final requerido.")]
        public int RpsFinal { get; set; }
        public int? RpsAtual { get; set; }
        public int RpsAviso { get; set; }
    }
}
using System;
using System.ComponentModel.DataAnnotations;

namespace Sagep.Application.ViewModels.RequestModels
{
    public class NfseSendRequestModel
    {
        [Required(ErrorMessage = "\nId requerido.")]
        public string Id { get; set; }

        [Required(ErrorMessage = "\nRPS requerida.")]
        public string Rps { get; set; }
    }
}
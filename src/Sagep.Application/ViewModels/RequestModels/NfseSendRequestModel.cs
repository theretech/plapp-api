using System;
using System.ComponentModel.DataAnnotations;

namespace Sagep.Application.ViewModels.RequestModels
{
    public class NfseCancelRequestModel
    {
        [Required(ErrorMessage = "\nId requerido.")]
        public string Id { get; set; }

        [Required(ErrorMessage = "\nCódigo cancelamento requerido.")]
        public string codigoCancelamento { get; set; }
    }
}
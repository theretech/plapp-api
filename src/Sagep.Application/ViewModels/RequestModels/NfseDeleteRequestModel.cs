using System;
using System.ComponentModel.DataAnnotations;

namespace Sagep.Application.ViewModels.RequestModels
{
    public class NfseDeleteRequestModel
    {
        [Required(ErrorMessage = "\nSequencial requerido.")]
        public int sequencial { get; set; }
    }
}
using System;

namespace Sagep.Application.ViewModels
{
    public class NotaFiscalViewModel
    {
        public string Id { get; set; }
        public string TenantId { get; set; }
        public string TomadorId { get; set; }
        public string NotaFiscalStatusId { get; set; }
        public string CodigoChamada { get; set; }
        public string MesChamada { get; set; }
        public Int32 CodigoEmpresa { get; set; }
        public Int32 CodigoConvenio { get; set; }
        public DateTime Competencia { get; set; }
        public Int32? Sequencial { get; set; }
        public Int32? NumeroNotaFiscal { get; set; }

        #pragma warning disable CS8632
        public string? InformacoesComplementares { get; set; }
        public string? CodigoCancelamento { get; set; }
        #pragma warning restore CS8632
    }
}
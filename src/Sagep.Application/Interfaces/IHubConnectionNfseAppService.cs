using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using Refit;
using Sagep.Application.ViewModels.RequestModels;
using Sagep.Application.ViewModels.ResponseModel;

namespace Sagep.Application.Interfaces
{
    public interface IHubConnectionNfseAppService
    {
        Task<IEnumerable<NfseListResponseModel>> ListAsync(NfseListRequestModel filters);
        Task<NfseListResponseModel> ListOneAsync(string id);
        Task<ResponseBaseModel> SyncAsync(Guid notaFiscalServicoId);
        Task<ResponseBaseModel> SendAsync(string id);
        Task<ResponseBaseModel> CancelAsync(NfseCancelRequestModel nfseCancelRequestModel);
        Task<ResponseBaseModel> DeleteAsync(NfseDeleteRequestModel nfseDeleteRequestModel);
        Task<Stream> ExportXmlAsync(NfseListRequestModel filters);
        Task<ApiResponse<Stream>> ExportPdfAsync(NfseListRequestModel filters);
        Task<Stream> ExportOnePdfAsync(string id);
        Task<Stream> ExportOneXmlAsync(string id);
        Task<ResponseBaseModel> SendEmailAsync(NfseSendEmailRequestModel filters);
        Task<NfseDashResponseModel> DashboardAsync();
        Task<IEnumerable<NfseConfigResponseModel>> ConfigAsync();
        Task<ResponseBaseModel> ConfigAddAsync(NfseConfigRequestModelAdd dataConfig);
        Task<ResponseBaseModel> ConfigUpdateAsync(NfseConfigRequestModelUpdate dataConfig);
        Task<ResponseBaseModel> ConfigDeleteAsync(string id);
        Task<string> ConfigWarningAsync();
    }
}
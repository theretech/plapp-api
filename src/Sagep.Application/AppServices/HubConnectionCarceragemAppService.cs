using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using Sagep.Application.HttpServices;
using Sagep.Application.Interfaces;
using Sagep.Application.ViewModels;
using Sagep.Domain.Security;

namespace Sagep.Application.Services
{
    public class HubConnectionCarceragemAppService : IHubConnectionCarceragemAppService
    {
        private readonly ILogger<HubConnectionCarceragemAppService> _logger;
        private readonly ISagepCarceragemServices _sagepCarceragemService;
        private readonly ITokenProvider _tokenProvider;
        

        public HubConnectionCarceragemAppService(ILogger<HubConnectionCarceragemAppService> logger,
                                                ISagepCarceragemServices sagepCarceragemService,
                                                ITokenProvider tokenProvider)
        {
            _logger = logger;
            _sagepCarceragemService = sagepCarceragemService;
            _tokenProvider = tokenProvider;
        }

        public async Task<IEnumerable<DetentoLegadoViewModel>> GetAllAsync()
        {
            var detentos = await _sagepCarceragemService.GetAllAsync($"Bearer {_tokenProvider.GetTokenFromHttpContext()}");
            return detentos;
        }
    }
}
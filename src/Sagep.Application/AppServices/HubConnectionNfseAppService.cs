using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;
using Microsoft.Extensions.Logging;
using Refit;
using Sagep.Application.HttpServices;
using Sagep.Application.Interfaces;
using Sagep.Application.ViewModels.RequestModels;
using Sagep.Application.ViewModels.ResponseModel;
using Sagep.Domain.Security;

namespace Sagep.Application.Services
{
    public class HubConnectionNfseAppService : IHubConnectionNfseAppService
    {
        private readonly ILogger<HubConnectionNfseAppService> _logger;
        private readonly ISagepNfseServices _sagepNfseService;
        private readonly ITokenProvider _tokenProvider;
        

        public HubConnectionNfseAppService(ILogger<HubConnectionNfseAppService> logger,
                                           ISagepNfseServices sagepNfseService,
                                           ITokenProvider tokenProvider)
        {
            _logger = logger;
            _sagepNfseService = sagepNfseService;
            _tokenProvider = tokenProvider;
        }

        public async Task<IEnumerable<NfseListResponseModel>> ListAsync(NfseListRequestModel filters)
        {
            var notasFiscais = await _sagepNfseService.ListAsync($"Bearer {_tokenProvider.GetTokenFromHttpContext()}", filters);
            return notasFiscais;
        }

        public async Task<NfseListResponseModel> ListOneAsync(string id)
        {
            var notaFiscal = await _sagepNfseService.ListOneAsync($"Bearer {_tokenProvider.GetTokenFromHttpContext()}", id);
            return notaFiscal;
        }

        public async Task<ResponseBaseModel> SyncAsync(Guid notaFiscalServicoId)
        {
            var result = await _sagepNfseService.SyncAsync($"Bearer {_tokenProvider.GetTokenFromHttpContext()}", notaFiscalServicoId.ToString());
            return result;
        }

        public async Task<ResponseBaseModel> SendAsync(string id)
        {
            var result = await _sagepNfseService.SendAsync($"Bearer {_tokenProvider.GetTokenFromHttpContext()}", id);
            return result;
        }

        public async Task<ResponseBaseModel> CancelAsync(NfseCancelRequestModel nfseCancelRequestModel)
        {
            var result = await _sagepNfseService.CancelAsync($"Bearer {_tokenProvider.GetTokenFromHttpContext()}", nfseCancelRequestModel);
            return result;
        }

        public async Task<ResponseBaseModel> DeleteAsync(NfseDeleteRequestModel NfseDeleteRequestModel)
        {
            var result = await _sagepNfseService.DeleteAsync($"Bearer {_tokenProvider.GetTokenFromHttpContext()}", NfseDeleteRequestModel);
            return result;
        }

        public async Task<Stream> ExportXmlAsync(NfseListRequestModel filters)
        {
            var result = await _sagepNfseService.ExportXmlAsync($"Bearer {_tokenProvider.GetTokenFromHttpContext()}", filters);
            return result;
        }

        public async Task<ApiResponse<Stream>> ExportPdfAsync(NfseListRequestModel filters)
        {
            var result = await _sagepNfseService.ExportPdfAsync($"Bearer {_tokenProvider.GetTokenFromHttpContext()}", filters);
            return result;            
        }

        public async Task<Stream> ExportOnePdfAsync(string id)
        {
            var result = await _sagepNfseService.ExportOnePdfAsync($"Bearer {_tokenProvider.GetTokenFromHttpContext()}", id, "pdf");
            return result;
        }

        public async Task<Stream> ExportOneXmlAsync(string id)
        {
            var result = await _sagepNfseService.ExportOneXmlAsync($"Bearer {_tokenProvider.GetTokenFromHttpContext()}", id, "xml");
            return result;
        }

        public async Task<ResponseBaseModel> SendEmailAsync(NfseSendEmailRequestModel filters)
        {
            var result = await _sagepNfseService.SendEmailAsync($"Bearer {_tokenProvider.GetTokenFromHttpContext()}", filters);
            return result;
        }

        public async Task<NfseDashResponseModel> DashboardAsync()
        {
            var result = await _sagepNfseService.DashboardAsync($"Bearer {_tokenProvider.GetTokenFromHttpContext()}");
            return result;
        }

        public async Task<IEnumerable<NfseConfigResponseModel>> ConfigAsync()
        {
            var result = await _sagepNfseService.ConfigAsync($"Bearer {_tokenProvider.GetTokenFromHttpContext()}");
            return result;
        }
        
        public async Task<ResponseBaseModel> ConfigAddAsync(NfseConfigRequestModelAdd dataConfig)
        {
            var result = await _sagepNfseService.ConfigAddAsync($"Bearer {_tokenProvider.GetTokenFromHttpContext()}", dataConfig);
            return result;
        }
        public async Task<ResponseBaseModel> ConfigUpdateAsync(NfseConfigRequestModelUpdate dataConfig)
        {
            var result = await _sagepNfseService.ConfigUpdateAsync($"Bearer {_tokenProvider.GetTokenFromHttpContext()}", dataConfig);
            return result;
        }
        public async Task<ResponseBaseModel> ConfigDeleteAsync(string id)
        {
            var result = await _sagepNfseService.ConfigDeleteAsync($"Bearer {_tokenProvider.GetTokenFromHttpContext()}", id);
            return result;
        }
        public async Task<string> ConfigWarningAsync()
        {
            var result = await _sagepNfseService.ConfigWarningAsync($"Bearer {_tokenProvider.GetTokenFromHttpContext()}");
            return result;
        }
    }
}
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using Sagep.Domain.Helpers;

namespace Sagep.Domain.Enums
{
    // Define an extension method in a non-nested static class.
    public static class Extensions
    {
        public static string GetDescription(this PermissionEnum pe)
        {
            return EnumHelper.GetDescription(pe);
        }

        public static List<string> GetNames()
        {
            return EnumHelper.GetNames<PermissionEnum>().ToList();
        }
    }

    public enum PermissionEnum
    {
        #region Master
        [Description("Pode realizar todas as ações/operações, bem como ter acesso a todos os dados e funcionalidades")]
        Master = 0,
        #endregion

        #region Dashboard Publica
        [Description("Pode realizar todas as ações/operações em dashboard publica")]
        CanDashboardPublicaAll = 1,
        #endregion

        #region Usuário
        [Description("Pode realizar todas as ações/operações relacionadas a entidade usuário")]
        CanUserAll = 100,
        [Description("Pode listar os dados de todos os usuários")]
        CanUserList = 101,
        [Description("Pode listar os dados de um usuários")]
        CanUserRead = 102,
        [Description("Pode criar um usuário")]
        CanUserCreate = 103,
        [Description("Pode atualizar os dados de um usuário")]
        CanUserUpdate = 104,
        [Description("Pode deletar um usuário")]
        CanUserDelete = 105,
        #endregion

        #region Roles (Permissões)
        [Description("Pode realizar todas as ações/operações em todos as roles/permissões")]
        CanRoleAll = 200,
        [Description("Pode listar os dados de todas as roles/permissões")]
        CanRoleList = 201,
        [Description("Pode listar os dados de uma roles/permissão")]
        CanRoleRead = 202,
        [Description("Pode criar uma role/permissão")]
        CanRoleCreate = 203,
        [Description("Pode atualizar os dados de uma roles/permissão")]
        CanRoleUpdate = 204,
        [Description("Pode deletar uma role/permissão")]
        CanRoleDelete = 205,
        #endregion

        #region Grupo de usuários
        [Description("Pode realizar todas as ações/operações em todos os grupos")]
        CanGroupAll = 300,
        [Description("Pode listar os dados de todos os grupos")]
        CanGroupList = 301,
        [Description("Pode listar os dado de um grupo")]
        CanGroupRead = 302,
        [Description("Pode criar um grupo")]
        CanGroupCreate = 303,
        [Description("Pode atualizar os dados de um grupo")]
        CanGroupUpdate = 304,
        [Description("Pode deletar um grupo")]
        CanGroupDelete = 305,
        #endregion

        #region Title
        [Description("Pode listar o título do sistema")]
        CanSectionTitleSystemList = 400,
        [Description("Pode listar o título Fiscal")]
        CanSectionTitleFiscalList = 401,
        #endregion

        #region Nota Fiscal Serviços
        [Description("Pode realizar todas as ações/operações em todas as NFS-e")]
        CanNfseAll = 500,
        [Description("Pode listar os dados de todas as NFS-e")]
        CanNfseList = 501,
        [Description("Pode visualizar uma NFS-e")]
        CanNfseRead = 502,
        [Description("Pode criar/importar uma NFS-e")]
        CanNfseCreate = 503,
        [Description("Pode atualizar uma NFS-e")]
        CanNfseUpdate = 504,
        [Description("Pode deletar uma NFS-e")]
        CanNfseDelete = 505,
        #endregion

        #region Permissões de EndPoint
        [Description("Permissão para acessar o EndPoint HubConnectionNfse")]
        CanHubConnectionNfseAccess = 600,

        [Description("Permissão para acessar o EndPoint HubConnectionCarceragem")]
        CanHubConnectionCarceragemAccess = 601
        #endregion 
    }
}